FROM alpine:3.15
RUN apk add --no-cache \
    nginx \
    python3 \
    py3-pip \
    curl \
    unzip \
    php7 \
    php7-fpm \
    php7-mbstring \
    php7-json \
    php7-xml \
    php7-dom \
    php7-curl \
    php7-exif \
    php7-gd \
    php7-iconv \
    php7-intl \
    php7-openssl \
    php7-pdo \
    php7-pdo_sqlite \
    php7-pdo_pgsql \
    php7-pecl-redis \
    php7-sodium \
    php7-tidy \
    php7-pecl-uuid \
    php7-zip \
 && rm /etc/nginx/http.d/default.conf \
 && rm /etc/php7/php-fpm.d/www.conf \
 && mkdir -p /run/nginx \
 && mkdir -p /var/www/snappymail \
 && mkdir -p /config \
 && pip3 install socrate==0.2.0

# nginx / PHP config files
COPY config/nginx-snappymail.conf /config/nginx-snappymail.conf
COPY config/php-snappymail.conf /etc/php7/php-fpm.d/snappymail.conf

# Snappymail login
COPY login/include.php /var/www/snappymail/include.php
COPY login/sso.php /var/www/snappymail/sso.php

# Parsing configs moved to startup
COPY defaults/php.ini /defaults/php.ini
COPY defaults/application.ini /defaults/application.ini
COPY defaults/default.ini /defaults/default.ini

ENV APP_VERSION 2.15.1
ENV SNAPPYMAIL_URL https://github.com/the-djmaze/snappymail/releases/download/v$APP_VERSION/snappymail-$APP_VERSION.zip

# Install Snappymail from source

RUN apk add --no-cache \
      curl unzip \
 && cd /var/www/snappymail \
 && curl -L -O ${SNAPPYMAIL_URL} \
 && unzip -q *.zip \
 && rm -f *.zip \
 && rm -rf data/ \
 && find . -type d -exec chmod 755 {} \; \
 && find . -type f -exec chmod 644 {} \; \
 && chown -R nginx:nginx /var/www/snappymail \
 && apk del unzip

COPY start.py /start.py
COPY config.py /config.py

EXPOSE 80/tcp
VOLUME ["/data"]

ENTRYPOINT ["/start.py"]

HEALTHCHECK CMD curl -f -L http://localhost/ || exit 1
